from Crypto.PublicKey import RSA
import Crypto.Hash.SHA384 as SHA384


class CertificatePrivate():
    def __init__(self, name, key):
        self.name = name
        self.key = key

    def sign(self, message):
        h = SHA384.new()
        h.update(message)
        return self.key.sign(h.digest(), 0)[0]

    def verify(self, message, signature):
        h = SHA384.new()
        h.update(message)
        message_sig = h.digest()
        return self.key.verify(message_sig, (signature, None))

    def to_public(self):
        return CertificatePublic(self.name, self.key.publickey())


class CertificatePublic():
    def __init__(self, name, key):
        self.name = name
        self.key = key.publickey()

    def verify(self, message, signature):
        h = SHA384.new()
        h.update(message)
        message_sig = h.digest()
        return self.key.verify(message_sig, (signature, None))


