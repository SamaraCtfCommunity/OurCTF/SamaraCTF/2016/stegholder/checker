import sqlite3
import os
import pickle
import base64
import SocketServer
import random
import hashlib
import struct
import logging

import cert_generation

db_file_path = 'db'

keys_number = 400
ADDRESS = '0.0.0.0'
PORT = int(os.getenv('DB_PORT', 8888))
logger = 0


def generate_db(db_file_path=db_file_path, n=keys_number):
    print 'Pre-generate public keys'
    if os.path.exists(db_file_path):
        os.remove(db_file_path)
    conn = sqlite3.connect(db_file_path)
    c = conn.cursor()
    c.execute(
        'CREATE TABLE keys (id INTEGER PRIMARY KEY, public_key TEXT NOT NULL, private_key TEXT NOT NULL);')
    for i in xrange(n):
        cert_priv = cert_generation.gen_checker_cert(get_random_user_name())
        print 'Generating certificate #{0} ...'.format(i),
        c.execute(
            "INSERT INTO keys VALUES ({0},'{1}','{2}');".format(i, base64.b64encode(
                pickle.dumps(cert_priv.to_public(), 2)), base64.b64encode(pickle.dumps(cert_priv, 2))))
        print 'Done'
    c.close()
    conn.commit()
    conn.close()


def get_cert(round_number):
    conn = sqlite3.connect(db_file_path)
    c = conn.cursor()
    N = -1
    for row in c.execute('SELECT Count() FROM keys;'):
        N = int(row[0])
    if round_number < 0:
        raise Exception('Negative round_number')
    if N <= 0:
        raise Exception('The database is empty')
    if round_number > N:
        raise Exception('The database is too small')

    round_number %= N
    res = None

    for row in c.execute("SELECT public_key FROM keys WHERE id='{0}';".format(round_number)):
        res = base64.b64decode(row[0].encode('ascii'))
    c.close()
    conn.commit()
    conn.close()
    return res


def get_private_cert(round_number):
    conn = sqlite3.connect(db_file_path)
    c = conn.cursor()
    N = -1
    for row in c.execute('SELECT Count() FROM keys;'):
        N = int(row[0])
    if round_number < 0:
        raise Exception('Negative round_number')
    if N <= 0:
        raise Exception('The database is empty')
    if round_number > N:
        raise Exception('The database is too small')

    round_number %= N
    res = None

    for row in c.execute("SELECT private_key FROM keys WHERE id='{0}';".format(round_number)):
        res = base64.b64decode(row[0].encode('ascii'))
    c.close()
    conn.commit()
    conn.close()
    return res


def get_random_user_name():
    names = ["Alice", "Bob", "Charlie", "Dan", "Elane", "Fred", "Greg"]
    return random.choice(names)


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass


class ServiceServerHandler(SocketServer.BaseRequestHandler):
    def __init__(self, request, client_address, server):
        SocketServer.BaseRequestHandler.__init__(self, request, client_address, server)

    def handle(self):
        try:
            request_str = read_message(self.request)
            pos = request_str.find('#')
            if pos == -1:
                raise Exception("Invalid message format")
            cmd, received_id = request_str[:pos], int(request_str[pos + 1:])
            if cmd != "GET":
                raise Exception("Invalid command")
            send_message(self.request, get_cert(received_id))
        except Exception as ex:
            logger.error(str(ex), exc_info = True)
        finally:
            pass
        return


def read_message(s):
    received_buffer = s.recv(4)
    if len(received_buffer) < 4:
        raise Exception('Error while receiving data')
    to_receive = struct.unpack('>I', received_buffer[0:4])[0]
    received_buffer = ''
    while len(received_buffer) < to_receive:
        received_buffer += s.recv(to_receive - len(received_buffer))
    return received_buffer


def send_message(s, message):
    send_buffer = struct.pack('>I', len(message)) + message
    s.sendall(send_buffer)


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.INFO)

    if not os.path.isfile(db_file_path):
        generate_db()
    address = (ADDRESS, PORT)
    server = ThreadedTCPServer(address, ServiceServerHandler)
    server.serve_forever()
