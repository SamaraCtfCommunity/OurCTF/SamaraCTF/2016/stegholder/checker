import cPickle as pickle
import os
import random
import socket
import struct
import base64
import requests
import ca

from Crypto.Cipher import ARC4
from Crypto.Hash import MD5
from themis.checker import Result, Server
import themis.checker

import tlsb
from cert_generation import gen_checker_cert

global SERVICE_PORT
global SOCKET_TIMEOUT
global KEYS_DB_PATH
global CHECKER_HOST_IP

SERVICE_PORT = int(os.getenv('SERVICE_PORT', 7777))
SOCKET_TIMEOUT = int(os.getenv('SOCKET_TIMEOUT', 20))
CHECKER_HOST_IP = os.getenv('CHECKER_HOST_IP', '127.0.0.1')

generator = 2
modulus = 0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA237327FFFFFFFFFFFFFFFF


class SampleChecker(Server):
    def push(self, team_ip, flag_id, flag):
        flag = flag.decode('utf-8').encode('ascii')

        try:
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(SOCKET_TIMEOUT)
                s.connect((team_ip, SERVICE_PORT))
            except Exception as ex:
                self.logger.error('Failed to connect to %s:%d', team_ip, SERVICE_PORT)
                self.logger.debug(str(ex), exc_info=True)
                return Result.DOWN, flag_id

            try:
                # print 'DEBUG: CHECKER_HOST_IP', CHECKER_HOST_IP
                # round_number = get_round_number(CHECKER_HOST_IP)  # todo remove #
                round_number = 1

                checker_cert = pickle.loads(ca.get_private_cert(2))
                session_key, nonce = log_in(s, checker_cert)
                file_name = get_file_name(round_number)
                unique_file_name = MD5.new(os.urandom(32)).hexdigest() + '_' + file_name

                temp_file_name = MD5.new(str(random.randint(0, 1000))).hexdigest()[:2] + '_' + file_name

                flag_id = base64.b64encode(temp_file_name + "\x00" + hex(round_number) + '\xff' + MD5.new(
                    str(random.randint(0, 1000))).hexdigest())

                tlsb.write(flag, file_name, unique_file_name)
                with open(unique_file_name, 'rb') as f:
                    file_str = f.read()
                os.remove(unique_file_name)

                message = checker_cert.name + ', ' + encrypt(
                    hex(nonce) + '\\||??//PUT\\\\.!.//' + temp_file_name + '\\\\.!.//' + file_str,
                    session_key)
                to_send = message + '||//||' + hex(checker_cert.sign(message))
                send_message(s, to_send)

                if read_message(s) != 'DONE!':
                    raise Exception('Wrong last message')
            except Exception as ex:
                self.logger.error('Failed to commit the protocol')
                self.logger.error(str(ex), exc_info=True)
                return Result.MUMBLE, flag_id
            return Result.UP, flag_id
        finally:
            if s: s.close()

    def pull(self, team_ip, flag_id, flag):
        try:

            flag = flag.decode('utf-8').encode('ascii')
            flag_id = base64.b64decode(flag_id)
            pos = flag_id.find('\x00')
            pos_end = flag_id.find('\xff')
            file_name, round_number = flag_id[:pos], int(flag_id[pos + 1:pos_end], 16)
            checker_cert = pickle.loads(ca.get_private_cert(round_number))

        except Exception as ex:
            self.logger.error('Failed to init certificate')
            self.logger.error(str(ex))
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(SOCKET_TIMEOUT)
            s.connect((team_ip, SERVICE_PORT))
        except Exception as ex:
            self.logger.error('Failed to connect to %s:%d', team_ip, SERVICE_PORT)
            self.logger.debug(str(ex), exc_info=True)
            return Result.DOWN
        try:
            session_key, nonce = log_in(s, checker_cert)
        except Exception as ex:
            self.logger.error('Failed to log in')
            return Result.MUMBLE
        try:
            message = checker_cert.name + ', ' + encrypt(hex(nonce) + '\\||??//GET\\\\.!.//' + file_name, session_key)
            to_send = message + '||//||' + hex(checker_cert.sign(message))
            send_message(s, to_send)
        except Exception as ex:
            self.logger.error('Failed to send GET command')
            return Result.MUMBLE
        try:
            encrypted = read_message(s)
        except Exception as ex:
            self.logger.error('Failed to retrieve the encrypted flag')
            return Result.MUMBLE

        try:
            decrypted = decrypt(encrypted, session_key)
            pos = decrypted.find(' ')
            if pos == -1:
                raise Exception
            file_name = decrypted[:pos]
            file_str = decrypted[pos + 1:]
        except Exception as ex:
            self.logger.error('Error while decrypting the encrypted flag')
            return Result.MUMBLE
        file_name = MD5.new(os.urandom(32)).hexdigest() + '_' + file_name
        with open(os.path.join(file_name), 'wb+') as f:
            f.write(file_str)
        try:
            decrypted_flag = tlsb.read(file_name)
            os.remove(file_name)
        except Exception as ex:
            self.logger.error('Error with file ' + file_name)
            self.logger.error(str(ex))
            return Result.MUMBLE

        if flag == decrypted_flag:
            return Result.UP
        else:
            return Result.CORRUPT


def log_in(socket, private_cert):
    # first stage
    session_key_client, exponent = generate_half_key()
    message = pickle.dumps(private_cert.to_public(), 2) + ', ' + hex(session_key_client)
    signature = private_cert.sign(message)
    to_send = message + '||' + hex(signature)
    send_message(socket, to_send)

    # third stage
    recv_message = read_message(socket)
    ind = recv_message.find('||')
    if ind == -1:
        raise CryptoException('Invalid message structure at stage #3')
    message = recv_message[:ind]

    (key_part_str, nonce_str, cert_str) = message.split(', ')
    session_key_serv = long(key_part_str, 16)
    nonce = long(nonce_str, 16)
    # server_cert = pickle.loads(cert_str)
    # signature = long(recv_message[ind + 2:], 16)

    # if not server_cert.verify(message, signature):
    #     raise CryptoException('Signature verification failed')

    session_key = pow(session_key_serv, exponent, modulus)

    return session_key, nonce


def encrypt(message, key):
    return ARC4.new(str(key)).encrypt(message)


def decrypt(encrypted, key):
    return ARC4.new(str(key)).decrypt(encrypted)


def generate_half_key():
    exponent = random.randint(1, modulus - 1)
    return pow(generator, exponent, modulus), exponent


class CryptoException(Exception):
    pass


def read_message(s):
    received_buffer = s.recv(4)
    if len(received_buffer) < 4:
        raise Exception('Error while receiving data ' + str(len(received_buffer)))
    to_receive = struct.unpack('>I', received_buffer[0:4])[0]
    received_buffer = ''
    while len(received_buffer) < to_receive:
        received_buffer += s.recv(to_receive - len(received_buffer))
    return received_buffer


def send_message(s, message):
    send_buffer = struct.pack('>I', len(message)) + message
    s.sendall(send_buffer)


def get_file_name(num):
    images = ["img0.png", "img1.png", "img2.png", "img3.png", "img4.png", "img5.png", "img6.png", "img7.png",
              "img8.png", "img9.png"]
    return images[num % len(images)]


def get_round_number(ip):
    r = requests.get('http://{0}/api/contest/round'.format(ip))
    return int(r.json()['value'])


my_checker = SampleChecker()
# my_checker.run()

# test
team_ip = '192.168.0.79'
import binascii

flag1 = binascii.hexlify(os.urandom(16)) + '='
flag2 = binascii.hexlify(os.urandom(16)) + '='
result1, flag_id1 = my_checker.push(team_ip, 0, flag1)
result2, flag_id2 = my_checker.push(team_ip, 0, flag2)
print result1
print result2

print "push success"
# result3, flag_id3 = my_checker.push(team_ip, 0, '31112222AAAABBBB33334444CCCCDDD=')
# result4, flag_id4 = my_checker.push(team_ip, 0, '41112222AAAABBBB33334444CCCCDDD=')
# result5, flag_id5 = my_checker.push(team_ip, 0, '51112222AAAABBBB33334444CCCCDDD=')
print my_checker.pull(team_ip, flag_id1, flag1)
print my_checker.pull(team_ip, flag_id2, flag2)
# print my_checker.pull(team_ip, flag_id3, '31112222AAAABBBB33334444CCCCDDD=')
# print my_checker.pull(team_ip, flag_id4, '41112222AAAABBBB33334444CCCCDDD=')
# print my_checker.pull(team_ip, flag_id5, '51112222AAAABBBB33334444CCCCDDD=')
