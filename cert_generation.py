import cPickle as pickle
import decimal
import gmpy2
import random
from gmpy2 import is_prime, invert
import certificate

from Crypto.PublicKey import RSA

import certificate


def gen_big_prime():
    while True:
        p = random.randint(2 ** 1000, 2 ** 1048)
        if is_prime(p):
            return p


def gen_checker_cert(name):
    p = gen_big_prime()
    q = gen_big_prime()

    while True:
        if p != q:
            break
        else:
            q = gen_big_prime()

    n = p * q

    limitD = (decimal.Decimal(n) * decimal.Decimal(0.33333333333333)).sqrt().sqrt()

    while True:
        d = random.randint(1, limitD / 2)
        if gmpy2.gcd(d, (p - 1) * (q - 1)) == 1:
            break

    e = invert(d, (p - 1) * (q - 1))
    key = RSA.construct((long(n), long(e), long(d)))

    return certificate.CertificatePrivate(name, key)


if __name__ == '__main__':
    message = "attack_at_dawn"

    cert_priv = gen_checker_cert("a")
    cert_pub = cert_priv.to_public()

    import wiener_attack

    recover = certificate.CertificatePrivate(cert_pub.name, wiener_attack.attack(cert_pub.key.e, cert_pub.key.n))

    i = 4134